# Teste de Lógica
Seu teste de lógica é composto com 64 questões. Para suas resposta, considere que esteja utilizando `javascript`

### Questão 1
```
3 + 5;
```

**Resposta:**



### Questão 2
```
2 * 2;
```

**Resposta:**



### Questão 3
```
var a = 1;
a + 3;
```

**Resposta:**



### Questão 4
```
var a = 4;
var b = 6;
a + b;
```

**Resposta:**



### Questão 5
```
var a = 'ig';
var b = 'nv';
a + b;
```

**Resposta:**



### Questão 6
```
var a = 'nq';
var b = '46';
a + b;
```

**Resposta:**



### Questão 7
```
var a = 'rv';
var b = '57';
a + b;
```

**Resposta:**



### Questão 8
```
var a = 'wi';
var b = 'wu';
a + b;
```

**Resposta:**



### Questão 9
```
var a = 'nq';
var b = '31';
a + b;
```

**Resposta:**



### Questão 10
```
var a = 'nr';
var b = '70';
a + b;
```

**Resposta:**



### Questão 11
```
var a = 'tt';
var b = '53';
a + b;
```

**Resposta:**



### Questão 12
```
var a = 'mih';
var b = 'x94';
a + b;
```

**Resposta:**



### Questão 13
```
var a = '3';
var b = '0';
a + b;
```

**Resposta:**



### Questão 14
```
var a = 0;
var b = 4;
a + b;
```

**Resposta:**



### Questão 15
```
var a = '3';
var b = '5';
a + b;
```

**Resposta:**



### Questão 16
```
var a = 2;
a = 0;
a + 3;
```

**Resposta:**



### Questão 17
```
var a = 2;
a = a + 1;
a + 0;
```

**Resposta:**



### Questão 18
```
function hello (a, b) {
    return a + b;
}
hello(3, 2);
```

**Resposta:**



### Questão 19
```
function hello (a, b) {
    return a * b;
}
hello(1, 2);
```

**Resposta:**



### Questão 20
```
function hello (a) {
    return a * a;
}
hello(4);
```

**Resposta:**



### Questão 21
```
function hello (a) {
    return a;
}
hello(8);
```

**Resposta:**



### Questão 22
```
function hello () {
    return 4;
}
hello() + 1;
```

**Resposta:**



### Questão 23
```
var a = 'vgugn';
a.length;
```

**Resposta:**



### Questão 24
```
var a = 'rhi';
a.length;
```

**Resposta:**



### Questão 25
```
var a = ['f', 'm', 1, 'g', 'r'];
a.length;
```

**Resposta:**



### Questão 26
```
var a = ['i', 'g', 3, 'g'];
a.length;
```

**Resposta:**



### Questão 27
```
var a = [9, 'p', 5];
a[0];
```

**Resposta:**



### Questão 28
```
var a = [8, 7, 2, 'h', 4, 3];
a[0];
```

**Resposta:**



### Questão 29
```
var a = ['k', 4, 'j', 'k'];
a[1];
```

**Resposta:**



### Questão 30
```
var a = ['j', 'r', 'n', 'j', 'k', 'q', 'r'];
a[3];
```

**Resposta:**



### Questão 31
```
var a = ['y', 9, 'w', 8];
a[1];
```

**Resposta:**



### Questão 32
```
var a = [5, 'i', 7];
a[1];
```

**Resposta:**



### Questão 33
```
var a = ['k', 'q', 'v', 'u'];
a.indexOf('k');
```

**Resposta:**



### Questão 34
```
3 === 23;
```

**Resposta:**



### Questão 35
```
19 === 25;
```

**Resposta:**



### Questão 36
```
19 !== 23;
```

**Resposta:**



### Questão 37
```
16 !== 9;
```

**Resposta:**



### Questão 38
```
25 === '25';
```

**Resposta:**



### Questão 39
```
4 === '4';
```

**Resposta:**



### Questão 40
```
6 === '14';
```

**Resposta:**



### Questão 41
```
2 < 10;
```

**Resposta:**



### Questão 42
```
var a = 9;
a === 9;
```

**Resposta:**



### Questão 43
```
var a = 4;
a !== 4;
```

**Resposta:**



### Questão 44
```
var a = 3;
a = 2;
a + 2;
```

**Resposta:**



### Questão 45
```
var a = 1;
a === 13;
a + 2;
```

**Resposta:**



### Questão 46
```
var a = 2;
a === 14;
a + 1;
```

**Resposta:**



### Questão 47
```
var a = 0;
a = 1;
a + 2;
```

**Resposta:**



### Questão 48
```
var a = 0;
a = 5;
a + 1;
```

**Resposta:**



### Questão 49
```
var a = 0;
if (1 < 2) {
    a = 3;
}
a + 3;
```

**Resposta:**



### Questão 50
```
var a = 0;
if (5 < 2) {
    a = 3;
}
a + 2;
```

**Resposta:**



### Questão 51
```
if (1 < 4) {
    var a = 4;
} else {
    var a = 6;
}
a + 3;
```

**Resposta:**



### Questão 52
```
if (5 < 2) {
    var a = 0;
} else {
    var a = 4;
}
a + 1;
```

**Resposta:**



### Questão 53
```
if (2 === 2) {
    var a = 4;
} else {
    var a = 2;
}
a + 2;
```

**Resposta:**



### Questão 54
```
if (2 < 12) {
    var a = 1;
} else if (6 < 1) {
    var a = 2;
} else {
    var a = 3;
}
a + 4;
```

**Resposta:**



### Questão 55
```
if (7 < 5) {
    var a = 4;
} else if (7 < 9) {
    var a = 6;
} else {
    var a = 2;
}
a + 2;
```

**Resposta:**



### Questão 56
```
if (1 < 6) {
    var a = 1;
} else if (1 < 6) {
    var a = 3;
} else {
    var a = 5;
}
a + 3;
```

**Resposta:**



### Questão 57
```
if (1 < 9) {
    var a = 4;
} else if (1 < 3) {
    var a = 2;
} else {
    var a = 6;
}
a + 1;
```

**Resposta:**



### Questão 58
```
function hi (a, b) {
    return a * b;
}
hi(0, 2);
```

**Resposta:**



### Questão 59
```
function hi (a, b) {
    if (a < b) {
        return a + b;
    } else {
        return a * b;
    }
}
hi(3, 2);
```

**Resposta:**



### Questão 60
```
function hi (a, b) {
    if (a < b) {
        return a + b;
    } else {
        return a * b;
    }
}
hi(3, 3);
```

**Resposta:**



### Questão 61
```
function hi (a, b) {
    return a * b;
}
function hello (a, b) {
    return a + b;
}
hi(2, 3) + hello(1, 3);
```

**Resposta:**



### Questão 62
```
function hi (a, b) {
    return a * b;
}
function hello (a, b) {
    return a + b;
}
var a = hi(3, 3);
hello(a, 4);
```

**Resposta:**



### Questão 63
```
function hi (a, b) {
    return a * b;
}
function hello (a, b) {
    return a + b;
}
hello(hi(2, 3), 3);
```

**Resposta:**



### Questão 64
```
function hi (a, b) {
    return a * b;
}
function hello (a, b) {
    return hi(a, b + 1);
}
hello(4, 3);
```

**Resposta:**



## Finalizando o projeto 😎
Envie suas respostas para o email [rh@elevenfinancial.com](mailto:rh@elevenfinancial.com).

Boa sorte 🤙🤙